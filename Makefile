eps:
	for svg in img/*.svg; do inkscape --export-type=png -d 300 $$svg; done;


clean:
	rm -f *.eps
	rm -f *.pdf;
	rm -f *.aux;
	rm -f *.log;
	rm -f *.aut
	rm -f *.tex
	rm -f *.toc
	rm -f *.dvi
	rm -f *.out
	rm img/*.eps
	rm img/*.pdf
	rm img/*.png

.ONESHELL:
strip-svg:

	@for svg2 in img/*.svg
	do
		scour -i $$svg2 -o $$svg2.tmp;
		mv $$svg2.tmp $$svg2;
	done;
